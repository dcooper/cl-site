;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-
(in-package :cl-user)

(asdf:defsystem cl-site-woo
  :name "cl-site-woo"
  :version "0.0.1"
  :maintainer "Common-Lisp.Net maintainers"
  :author "Dave Cooper & Common-Lisp.Net maintainers"
  :licence "TBD"
  :description "Test server for common-lisp.net for Woo"
  :depends-on (:cl-site :woo) 
  :serial t
  :components ((:file "package")
	       (:file "publish")))

