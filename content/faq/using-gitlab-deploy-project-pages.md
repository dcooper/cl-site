title: Using GitLab to deploy project pages -- Common-Lisp.net FAQ
---

{table-of-contents :depth 2 :start 2 :label "Table of contents" }

# Using GitLab to deploy project pages

This page contains instructions on how to deploy your project pages
(those hosted on 'https://common-lisp.net/project/<your-project>`).

Using deployment through GitLab, you can use your preferred site
generator in your preferred setup: from "simply copy the repository
content" to builds depending on specialized Docker images, all these
are supported by our common-lisp.net setup.

## Step 1: Create a repository for your project pages

In order to deploy your project pages to
`https://common-lisp.net/project/<project>`, you need a
project in GitLab named `<project>-site` in the group named
`<project>`. That is, the resulting repository will be viewable
at the URL `https://gitlab.common-lisp.net/<project>/<project>-site`.


## Step 2: Add content to the repository

After the repository has been created, the site content can be
added. Take into account that any content that needs to be deployed
should end up in a subdirectory called `public/`.

A very simple example of deploying content straight from the repository
is the [cl-couch-site](https://gitlab.common-lisp.net/cl-couch/cl-couch-site/)
project. It holds the content to be deployed in a `src/` subdirectory
which can simply be renamed to `public/`. No further processing is
required.

More complex build steps can be found in the [clo/
cl-site](https://gitlab.common-lisp.net/clo/cl-site/) and [quickref/
quickref](https://gitlab.common-lisp.net/quickref/quickref/) projects
respectively.


## Step 3: Add continuous integration through `.gitlab-ci.yml`

Once a repository has been set up and seeded with content, deployment
instructions have to be added through GitLab's CI (Continuous Integration)
platform. The example below is the simplest deployment CI instruction,
deploying static site content without processing.

    pages:
      stage: deploy
      script: mv src public
      artifacts:
         paths:
           - public
      tags:
        - site-gen
      only:
        - master

The above defines the special purpose `pages` build job known to
GitLab to be associated with GitLab Pages deployment. GitLab CI
jobs can be tied to various "stages". The `pages` job should be
tied to the "deploy" stage. And last but not least, the job creates
artifacts in the `public` path which GitLab Pages uses to deploy
the site.

To prevent every intermediate result on every branch from being
deployed to your project site, the job is restricted to building
from the `master` branch only, using the

    only:
      - master

fragment.

The `tags` fragment mentioned below, links the job to CI builders
("runners"). Builders can only build jobs for which the job at
least one of the tags the builder has as well. On the common-lisp.net
infrastructure, this means that a job with the tag `site-gen` can
only be built with the builder tagged with the `site-gen` tag too.

    tags:
      - site-gen

Then, last but not least, the fragment `script: mv src public`
instructs the CI system to perform the "build" for this repository
(i.e. nothing more than to rename `src` to `public`).

As mentioned in the previous section, other, more complex, setups
and build instructions are possible. See for examples the [clo/
cl-site](https://gitlab.common-lisp.net/clo/cl-site/) and [quickref/
quickref](https://gitlab.common-lisp.net/quickref/quickref/) projects.
Note however, that these projects don't deploy to a `/project/<project>`
page (but to `https://common-lisp.net` and `https://quickref.common-lisp.net`
instead).


## Step 4: Deploy and test your new project pages

If the syntax of your `.gitlab-ci.yml` file is correct, a new build
will run on every push or merge to the master branch. With each successful
build, a new deployment will take place.

Note that if you want to check that your `.gitlab-ci.yml` file has
valid syntax, you can use GitLab's "CI Lint" application. This application
is available in every project on the "Pipelines" page. For the "clo/cl-site"
project, it's located at https://gitlab.common-lisp.net/clo/cl-site/-/ci/lint

Please note that if you're migrating existing project pages from
`/project/<project>/public_html` to GitLab based deployments that
your new site won't show up until the `public_html` directory has
been removed (or renamed).

For more information on how to use GitLab Pages and GitLab CI,
please see https://docs.gitlab.com/ce/user/project/pages/#overview .
